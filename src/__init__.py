import os

from flask import Flask
from flask_sqlalchemy import SQLAlchemy


# Instantiate db
db = SQLAlchemy()


def create_app(script_info=None):
    # Instantiate app
    app = Flask(__name__)

    # Set config
    app_settings = os.getenv('APP_SETTINGS')
    app.config.from_object(app_settings)

    db.init_app(app)

    # Register blueprints
    from src.api.ping import ping_blueprint
    app.register_blueprint(ping_blueprint)

    from src.api.users import users_blueprint
    app.register_blueprint(users_blueprint)

    # Shell context for flask cli
    @app.shell_context_processor
    def ctx():
        return {'app': app, 'db': db}

    return app

# import os 

# from flask import Flask, jsonify
# from flask_restx import Resource, Api
# from flask_sqlalchemy import SQLAlchemy

# app = Flask(__name__)

# api = Api(app)

# # Set config
# app_settings = os.getenv('APP_SETTINGS')
# app.config.from_object(app_settings)

# # Instantiate db
# db = SQLAlchemy(app)


# # Model
# class User(db.Model):
#     __tablename__ = 'users'
#     id = db.Column(db.Integer, primary_key=True, autoincrement=True)
#     username = db.Column(db.String(128), nullable=False)
#     email = db.Column(db.String(128), nullable=False)
#     active = db.Column(db.Boolean(), default=True, nullable=False)

#     def __init__(self, username, email):
#         self.username = username
#         self.email = email

# class Ping(Resource):
#     def get(self):
#         return {
#             'status': 'success',
#             'message': 'pong!',
#         }


# api.add_resource(Ping, '/ping')